INSERT INTO formacao
(id, nivel, nome)
VALUES(1, 'SUPERIOR_COMPLETO', 'Fisioterapia');
INSERT INTO formacao
(id, nivel, nome)
VALUES(2, 'POS_GRADUADO', 'Fisioterapia Esportiva');
INSERT INTO formacao
(id, nivel, nome)
VALUES(3, 'MESTRE', 'Traumato-ortopédica');

INSERT INTO pessoa
(id, cidade, cpf, nome, rg, sexo, site_pessoal, telefone, uf)
VALUES(1, 'goiania', '11111', 'Joao', '1111', 'M', '', '', '');
INSERT INTO pessoa
(id, cidade, cpf, nome, rg, sexo, site_pessoal, telefone, uf)
VALUES(2, 'sao paulo', '1111', 'Miguel', '111', 'M', '', '', '');


INSERT INTO pessoa_formacoes
(pessoa_id, formacoes_id)
VALUES(1, 1);
INSERT INTO pessoa_formacoes
(pessoa_id, formacoes_id)
VALUES(1, 2);
INSERT INTO pessoa_formacoes
(pessoa_id, formacoes_id)
VALUES(1, 2);


INSERT INTO usuario
(id, eh_usuario_ativo, eh_usuario_desbloqueado, email, senha, dados_id)
VALUES(1, 1, 1, 'meuemail1@email.com', '$2a$10$HqtqX77LWod82tSlhtB3Vued62VDKl95pl8aV1IKYFO7IPBBP7SLm', 1);
INSERT INTO usuario
(id, eh_usuario_ativo, eh_usuario_desbloqueado, email, senha, dados_id)
VALUES(2, 1, 1, 'meuemail2@email.com', '$2a$10$r9OV17k4hUr.MK0N1Xxg.eebmyccHekEHrbITNdMiqd0PCiPxjcz6', 2);

INSERT INTO publicacao
(id, data_alteracao, data_publicacao, texto, autor_id)
VALUES(1, NULL, '2020-01-01 00:00:00', '	Lorem ipsum aliquam sed in cras eu bibendum ligula, nam rutrum ante sollicitudin aliquet scelerisque aenean, luctus hac porta aliquam venenatis habitasse litora. metus eu malesuada lacus adipiscing in primis pharetra leo, fringilla a tempus quisque vehicula consectetur adipiscing aptent, vulputate ultrices cursus tortor sed aenean laoreet. potenti vestibulum tellus conubia ante egestas praesent facilisis purus venenatis, feugiat leo dui elementum id class purus mi vulputate, nullam eleifend est sem placerat est augue faucibus. sapien lacus netus ipsum cursus maecenas orci aptent metus, mollis integer dolor sem turpis nisl congue sociosqu aptent, fringilla pellentesque quisque nunc aenean fusce iaculis. ', 1);
INSERT INTO publicacao
(id, data_alteracao, data_publicacao, texto, autor_id)
VALUES(2, NULL, '2020-01-02 00:00:00', '	Lorem ipsum dictum at id, dictum fringilla semper justo fermentum, risus erat integer. fringilla malesuada lectus nostra posuere donec habitant nostra ultricies tellus hendrerit tincidunt, pulvinar turpis litora risus lobortis luctus porttitor netus vivamus fusce odio, consequat dolor eget nam feugiat amet dui potenti sollicitudin morbi. tempor class neque auctor sem aliquet vestibulum suscipit ut pulvinar, class sem taciti vulputate phasellus vestibulum sed nullam, molestie lacinia porta cras nam ante mauris aliquet. aenean cras velit donec inceptos faucibus nulla class amet, eu nisi etiam amet habitant netus et, phasellus consequat ligula lacus maecenas duis hac. ', 1);
INSERT INTO publicacao
(id, data_alteracao, data_publicacao, texto, autor_id)
VALUES(3, NULL, '2020-01-02 00:00:00', '	Lorem ipsum dictum at id, dictum fringilla semper justo fermentum, risus erat integer. fringilla malesuada lectus nostra posuere donec habitant nostra ultricies tellus hendrerit tincidunt, pulvinar turpis litora risus lobortis luctus porttitor netus vivamus fusce odio, consequat dolor eget nam feugiat amet dui potenti sollicitudin morbi. tempor class neque auctor sem aliquet vestibulum suscipit ut pulvinar, class sem taciti vulputate phasellus vestibulum sed nullam, molestie lacinia porta cras nam ante mauris aliquet. aenean cras velit donec inceptos faucibus nulla class amet, eu nisi etiam amet habitant netus et, phasellus consequat ligula lacus maecenas duis hac. ', 2);

INSERT INTO area_atuacao
(id, nome)
VALUES(1, 'Saúde esportiva');
INSERT INTO area_atuacao
(id, nome)
VALUES(2, 'Traumas');

INSERT INTO publicacao_areas
(publicacao_id, areas_id)
VALUES(1, 1);
INSERT INTO publicacao_areas
(publicacao_id, areas_id)
VALUES(1, 2);
INSERT INTO publicacao_areas
(publicacao_id, areas_id)
VALUES(2, 1);

INSERT INTO comentario
(id, data_alteracao, data_criacao, texto, publicacao_id, usuario_id)
VALUES(1, NULL, '2020-01-01 00:00:00', 'Comentário 1', 1, 1);
INSERT INTO comentario
(id, data_alteracao, data_criacao, texto, publicacao_id, usuario_id)
VALUES(2, NULL, '2020-01-01 00:00:00', 'Comentário 2', 1, 1);

INSERT INTO reacao
(id, nome, publicacao_id, usuario_id)
VALUES(1, 'LIKED', 1, 1);


