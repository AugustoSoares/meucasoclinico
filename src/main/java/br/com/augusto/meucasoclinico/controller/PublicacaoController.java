package br.com.augusto.meucasoclinico.controller;


import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.augusto.meucasoclinico.dto.PublicacaoDto;
import br.com.augusto.meucasoclinico.dto.form.PublicacaoForm;
import br.com.augusto.meucasoclinico.dto.update.PublicacaoFormUpdate;
import br.com.augusto.meucasoclinico.filtro.FiltroListaPublicacao;
import br.com.augusto.meucasoclinico.model.Publicacao;
import br.com.augusto.meucasoclinico.repository.PublicacaoRepository;

@RestController
@RequestMapping("/publicacoes")
public class PublicacaoController {

	@Autowired
	PublicacaoRepository publicacaoRepository;
	
	@GetMapping("/lista-paginada")
	@Cacheable("listaPublicacoes")
	public Page<PublicacaoDto> getPublicacoes(@PageableDefault(page = 0, 
															   size = 15,
															   sort = "id", 
															   direction = Direction.DESC) Pageable paginacao,
									  		  @RequestParam(required = false) String nomeAutor) {
		
		if (nomeAutor != null) {
			return PublicacaoDto.converte(publicacaoRepository.findByAutorDadosNome(nomeAutor, paginacao));
		} else {
			return PublicacaoDto.converte(publicacaoRepository.findAll(paginacao));
		}

	}
	
	@GetMapping("/lista-paginada-com-criteria")
	@Transactional
	public Page<PublicacaoDto> getPublicacoesCriteria(@PageableDefault(page = 0, size = 15) Pageable paginacao,
													  HttpServletRequest request) {

		return PublicacaoDto.converte(publicacaoRepository.getListaPaginada(new FiltroListaPublicacao(request), paginacao));
	} 

	@PostMapping
	@Transactional
	@CacheEvict(value = "listaPublicacoes", allEntries = true)
	public ResponseEntity<PublicacaoDto> cadastra(@RequestBody @Valid PublicacaoForm form, 
											  	  UriComponentsBuilder uriBuilder) {

		Publicacao publicacao = form.converte();
		publicacaoRepository.save(publicacao);

		URI uri = uriBuilder.path("/publicacao/{id}").buildAndExpand(publicacao.getId()).toUri();

		return ResponseEntity.created(uri).body(new PublicacaoDto(publicacao));
	}

	@PutMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaPublicacoes", allEntries = true)
	public ResponseEntity<PublicacaoDto> atualiza(@PathVariable long id, 
											  	  @Valid @RequestBody PublicacaoFormUpdate form) {
		
		Optional<Publicacao> optional = publicacaoRepository.findById(id);
		if (optional.isPresent()) {
			Publicacao publicacao = form.atualiza(optional.get());
			return ResponseEntity.ok(new PublicacaoDto(publicacao));
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaPublicacoes", allEntries = true)
	public ResponseEntity<?> remove(@PathVariable long id) {
		
		Optional<Publicacao> optional = publicacaoRepository.findById(id);
		if (optional.isPresent()) {
			publicacaoRepository.deleteById(id); 
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.notFound().build();
	}

}
