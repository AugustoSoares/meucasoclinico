package br.com.augusto.meucasoclinico.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.augusto.meucasoclinico.config.security.TokenService;
import br.com.augusto.meucasoclinico.dto.TokenDto;
import br.com.augusto.meucasoclinico.dto.form.UsuarioLoginForm;

@RestController
@RequestMapping("/auth")
public class AutenticacaoController {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenService tokenService;

	@PostMapping
	public ResponseEntity<?> autentica(@RequestBody @Valid UsuarioLoginForm form) {

		UsernamePasswordAuthenticationToken dadosLogin = form.converte();

		try {
			Authentication authentication = authenticationManager.authenticate(dadosLogin);
			String token = tokenService.geraToken(authentication);
			
			return ResponseEntity.ok(new TokenDto(token, TokenService.AUTHORIZATION_TYPE));
			
		} catch (AuthenticationException ex) {
			return ResponseEntity.badRequest().build();
		}

	}

}
