package br.com.augusto.meucasoclinico.dto;

import org.springframework.data.domain.Page;

import br.com.augusto.meucasoclinico.model.Publicacao;

public class PublicacaoDto {

	private Long id;
	private String texto;
	private String nomeAutor;
	private String observacoes;

	public PublicacaoDto(Publicacao publicacao) {
		this.id = publicacao.getId();
		this.texto = publicacao.getTexto();
		this.nomeAutor = publicacao.getNomeAutor();
		this.observacoes = publicacao.getObservacoes();
	}

	public Long getId() {
		return id;
	}

	public String getTexto() {
		return texto;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public static Page<PublicacaoDto> converte(Page<Publicacao> publicacoes) {
		return publicacoes.map(PublicacaoDto::new);
	}

	

}
