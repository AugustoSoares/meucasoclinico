package br.com.augusto.meucasoclinico.dto.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.augusto.meucasoclinico.config.security.UserLogged;
import br.com.augusto.meucasoclinico.model.Publicacao;

public class PublicacaoForm {

	@NotNull
	@NotEmpty
	@Length(min = 30, max = 150)
	private String titulo;

	@NotNull
	@NotEmpty
	private String texto;

	private String observacoes;

	public String getTitulo() {
		return titulo;
	}

	public String getTexto() {
		return texto;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public Publicacao converte() {
		return new Publicacao(titulo, texto, observacoes, UserLogged.getUsuario());
	}

}
