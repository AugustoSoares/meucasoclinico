package br.com.augusto.meucasoclinico.dto.update;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.augusto.meucasoclinico.model.Publicacao;

public class PublicacaoFormUpdate {

	@NotNull
	@NotEmpty
	@Length(min = 30, max = 150)
	private String texto;

	@NotNull
	@NotEmpty
	private String observacoes;

	public String getTexto() {
		return texto;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public Publicacao atualiza(Publicacao publicacao) {
		publicacao.setTexto(texto);
		publicacao.setObservacoes(observacoes);
		
		return publicacao;
	}

}
