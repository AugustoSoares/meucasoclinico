package br.com.augusto.meucasoclinico.dto.form;

import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

public class UsuarioForm {
	
	@NotNull
	@NotEmpty
	private String email;
	
	@NotNull
	@NotEmpty
	private String senha;
	
	@NotNull
	private Integer codigoPessoa;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Integer getCodigoPessoa() {
		return codigoPessoa;
	}

	public void setCodigoPessoa(Integer codigoPessoa) {
		this.codigoPessoa = codigoPessoa;
	}
	

}
