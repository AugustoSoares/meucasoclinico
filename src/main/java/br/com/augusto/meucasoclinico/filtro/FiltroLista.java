package br.com.augusto.meucasoclinico.filtro;

import java.time.LocalDateTime;

public class FiltroLista {

	private Long id;
	private LocalDateTime dataInicial;
	private LocalDateTime dataFinal;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(LocalDateTime dataInicial) {
		this.dataInicial = dataInicial;
	}

	public LocalDateTime getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDateTime dataFinal) {
		this.dataFinal = dataFinal;
	}

}
