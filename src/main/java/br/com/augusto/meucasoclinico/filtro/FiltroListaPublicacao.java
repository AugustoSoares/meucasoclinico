package br.com.augusto.meucasoclinico.filtro;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class FiltroListaPublicacao extends FiltroLista {

	private Long idAutor;
	private String nomeAutor;
	private String titulo;

	public FiltroListaPublicacao(HttpServletRequest request) {
		this.idAutor = Long.parseLong(StringUtils.defaultString(request.getParameter("idAutor"), "-1"));
		this.nomeAutor = StringUtils.defaultString(request.getParameter("nomeAutor"), "");
		this.titulo = StringUtils.defaultString(request.getParameter("titulo"), "");
	}

	public Long getIdAutor() {
		return idAutor;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public String getTitulo() {
		return titulo;
	}

}
