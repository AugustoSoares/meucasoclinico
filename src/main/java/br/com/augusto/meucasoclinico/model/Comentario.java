package br.com.augusto.meucasoclinico.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Comentario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String texto;
	
	@ManyToOne
	private Publicacao publicacao;
	
	@ManyToOne
	private Usuario usuario;
	
	private LocalDateTime dataCriacao;
	private LocalDateTime dataAlteracao;
	
	public Comentario() {
		this.dataCriacao = LocalDateTime.now();
		this.dataAlteracao = LocalDateTime.now();
	}
	
}
