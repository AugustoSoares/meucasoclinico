package br.com.augusto.meucasoclinico.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nome;
	private String cpf;
	private String rg;
	private String uf;
	private String cidade;
	private String sitePessoal;
	private String telefone;

	@Column(length = 1)
	private String sexo;

	@ManyToMany
	private List<Formacao> formacoes;

	public Pessoa() {
		this.formacoes = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

	public String getRg() {
		return rg;
	}

	public String getUf() {
		return uf;
	}

	public String getCidade() {
		return cidade;
	}

	public String getSitePessoal() {
		return sitePessoal;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getSexo() {
		return sexo;
	}

	public List<Formacao> getFormacoes() {
		return formacoes;
	}

}
