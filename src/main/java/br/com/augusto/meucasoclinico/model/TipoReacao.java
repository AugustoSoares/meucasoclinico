package br.com.augusto.meucasoclinico.model;

public enum TipoReacao {
	LIKED, CONGRATULATED, LOVED, INTERESTED
}
