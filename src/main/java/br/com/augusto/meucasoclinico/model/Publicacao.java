package br.com.augusto.meucasoclinico.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Publicacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String titulo;

	@Lob
	private String texto;

	@Lob
	private String observacoes;

	@ManyToOne
	private Usuario autor;

	@ManyToMany(fetch = FetchType.LAZY)
	private List<AreaAtuacao> areas;

	@OneToMany(mappedBy = "publicacao", cascade = CascadeType.ALL)
	private List<Comentario> comentarios;

	@OneToMany(mappedBy = "publicacao", cascade = CascadeType.ALL)
	private List<Reacao> reacoes = new ArrayList<>();

	private LocalDateTime dataPublicacao;
	private LocalDateTime dataAlteracao;

	public Publicacao() {
		this.areas = new ArrayList<>();
		this.comentarios = new ArrayList<>();
		this.dataPublicacao = LocalDateTime.now();
	}

	public Publicacao(String titulo, String texto, String observacoes, Usuario autor) {
		this.areas = new ArrayList<>();
		this.comentarios = new ArrayList<>();
		this.dataPublicacao = LocalDateTime.now();
		this.titulo = titulo;
		this.texto = texto;
		this.observacoes = observacoes;
		this.autor = autor;
	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTexto() {
		return texto;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public Usuario getAutor() {
		return autor;
	}

	public List<AreaAtuacao> getAreas() {
		return areas;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public List<Reacao> getReacoes() {
		return reacoes;
	}

	public LocalDateTime getDataPublicacao() {
		return dataPublicacao;
	}

	public LocalDateTime getDataAlteracao() {
		return dataAlteracao;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getNomeAutor() {
		return this.autor.getDados().getNome();
	}

}
