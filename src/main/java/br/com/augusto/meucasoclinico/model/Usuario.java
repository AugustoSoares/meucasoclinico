package br.com.augusto.meucasoclinico.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
public class Usuario implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String email;
	private String senha;

	@OneToOne
	private Pessoa dados;
 
	private Boolean ehUsuarioDesbloqueado;
	private Boolean ehUsuarioAtivo;

	@ManyToMany(fetch = FetchType.EAGER)
	private List<DireitoUsuario> direitos;

	public Usuario() {
		this.direitos = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}

	public Boolean getEhUsuarioDesbloqueado() {
		return ehUsuarioDesbloqueado;
	}

	public Boolean getEhUsuarioAtivo() {
		return ehUsuarioAtivo;
	}

	public List<DireitoUsuario> getDireitos() {
		return direitos;
	}

	public Pessoa getDados() {
		return dados;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return direitos;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return ehUsuarioDesbloqueado;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return ehUsuarioAtivo;
	}
	
	public String getNomeUsuario() {
		return this.dados.getNome();
	}

}
