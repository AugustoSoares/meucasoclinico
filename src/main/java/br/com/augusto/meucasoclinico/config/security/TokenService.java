package br.com.augusto.meucasoclinico.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.augusto.meucasoclinico.model.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	@Value("${app.jwt.expiration}")
	private String expiration;
	
	@Value("${app.jwt.secret}")
	private String secret;

	public static final String AUTHORIZATION_TYPE = "Bearer";

	public String geraToken(Authentication authentication) {
		Usuario usuarioSolicitante = (Usuario) authentication.getPrincipal();
		Date dataAtual = new Date(System.currentTimeMillis());
		Date dataExpiracao = new Date(dataAtual.getTime() + Long.parseLong(expiration));
		
		return Jwts.builder().setIssuer("meu_caso_clinico_app")
							 .setSubject(usuarioSolicitante.getId().toString())
							 .setIssuedAt(dataAtual)
							 .setExpiration(dataExpiracao)
							 .signWith(SignatureAlgorithm.HS256, secret)
							 .compact();
		
	}

	private Jws<Claims> getClaims(String token) {
		return Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
	}
	
	public boolean validaToken(String token) {
		try {
			getClaims(token);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public long getIdUsuario(String token) {
		try {
			return Long.parseLong(getClaims(token).getBody().getSubject());
		} catch (Exception e) {
			return -1L;
		}
	}
	
}
