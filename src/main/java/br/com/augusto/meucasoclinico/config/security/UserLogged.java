package br.com.augusto.meucasoclinico.config.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.augusto.meucasoclinico.model.DireitoUsuario;
import br.com.augusto.meucasoclinico.model.Usuario;

public class UserLogged {

	private UserLogged() {
		throw new IllegalStateException("Classe utilitária");
	}

	private static SecurityContext getSecurityContext() {
		return SecurityContextHolder.getContext();
	}

	public static Usuario getUsuario() {
		return (Usuario) getSecurityContext().getAuthentication().getPrincipal();
	}

	public static long getIdUsuario() {
		return ((Usuario) getSecurityContext().getAuthentication().getPrincipal()).getId();
	}

	public static String getNomeUsuario() {
		return ((Usuario) getSecurityContext().getAuthentication().getPrincipal()).getNomeUsuario();
	}

	public static List<DireitoUsuario> getDireitos() {
		return ((Usuario) getSecurityContext().getAuthentication().getPrincipal()).getDireitos();
	}

	public static Collection<? extends GrantedAuthority> getAuthorities() {
		return ((Usuario) getSecurityContext().getAuthentication().getPrincipal()).getAuthorities();
	}

}
