package br.com.augusto.meucasoclinico.config.security;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.augusto.meucasoclinico.model.Usuario;
import br.com.augusto.meucasoclinico.repository.UsuarioRepository;

public class AuthenticationTokenFilter extends OncePerRequestFilter {
	
	private TokenService tokenService;
	private UsuarioRepository repository;

	public AuthenticationTokenFilter(TokenService tokenService , UsuarioRepository repository) {
		this.tokenService = tokenService;
		this.repository = repository;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request,
									HttpServletResponse response,
									FilterChain filterChain) throws ServletException, IOException {

		String token = recuperaToken(request);
		if (tokenService.validaToken(token)) {
			autentincaCliente(token);
		}
		
		filterChain.doFilter(request, response);
	}


	private String recuperaToken(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		String tipoAutorizacao = TokenService.AUTHORIZATION_TYPE + StringUtils.SPACE;
		
		if (token == null || token.isEmpty() || !token.startsWith(tipoAutorizacao)) {
			return null;
		}
		
		return token.substring(tipoAutorizacao.length(), token.length());
	}

	private void autentincaCliente(String token) {
		long idUsuario = tokenService.getIdUsuario(token);
		Optional<Usuario> usuarioOptional = repository.findById(idUsuario);
			
		if (usuarioOptional.isPresent()) {
			Usuario usuario = usuarioOptional.get();
			UsernamePasswordAuthenticationToken authentication = 
					new UsernamePasswordAuthenticationToken(usuario,
															usuario.getPassword(),
															usuario.getAuthorities());
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}
	}
}
