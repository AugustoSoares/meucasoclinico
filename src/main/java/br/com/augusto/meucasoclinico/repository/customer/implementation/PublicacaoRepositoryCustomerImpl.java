package br.com.augusto.meucasoclinico.repository.customer.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.augusto.meucasoclinico.filtro.FiltroListaPublicacao;
import br.com.augusto.meucasoclinico.model.Pessoa;
import br.com.augusto.meucasoclinico.model.Publicacao;
import br.com.augusto.meucasoclinico.model.Usuario;
import br.com.augusto.meucasoclinico.repository.customer.PublicacaoRepositoryCustomer;

@Service
public class PublicacaoRepositoryCustomerImpl implements PublicacaoRepositoryCustomer {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Page<Publicacao> getListaPaginada(FiltroListaPublicacao filtro, Pageable paginacao) {
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Publicacao> query = criteriaBuilder.createQuery(Publicacao.class);
		Root<Publicacao> root = query.from(Publicacao.class);

		List<Predicate> predicates = getPredicates(filtro, criteriaBuilder, root);
		query.where(predicates.toArray(new Predicate[0]));
		
        List<Publicacao> publicacoes = getListaPublicacoesFiltrada(paginacao, query);

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<Publicacao> publicacoesRootCount = countQuery.from(Publicacao.class);
        countQuery.select(criteriaBuilder.count(publicacoesRootCount))
        			.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        return getListaPageable(paginacao, publicacoes);
	}

	private PageImpl<Publicacao> getListaPageable(Pageable paginacao, List<Publicacao> publicacoes) {
		return new PageImpl<>(publicacoes, paginacao, paginacao.getPageSize());
	}

	private List<Publicacao> getListaPublicacoesFiltrada(Pageable paginacao, CriteriaQuery<Publicacao> query) {
		List<Publicacao> publicacoes = em.createQuery(query)
        		.setFirstResult((int) paginacao.getOffset())
        		.setMaxResults(paginacao.getPageSize())
        		.getResultList();
		
		return publicacoes;
	}

	private List<Predicate> getPredicates(FiltroListaPublicacao filtro,
										  CriteriaBuilder criteriaBuilder,
										  Root<Publicacao> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		Join<Publicacao, Usuario> usuario = root.join("autor");
		Join<Usuario, Pessoa> pessoa = usuario.join("dados");

		if (!StringUtils.isEmpty(filtro.getNomeAutor())) {
			predicates.add(criteriaBuilder.like(pessoa.get("nome"), "%" + filtro.getNomeAutor() + "%"));
		}

		if (filtro.getIdAutor() != null && filtro.getIdAutor() > 0) { 
			predicates.add(criteriaBuilder.equal(pessoa.get("id"), filtro.getIdAutor()));
		}

		if (!StringUtils.isEmpty(filtro.getTitulo())) { 
			predicates.add(criteriaBuilder.like(root.<String>get("titulo"), "%" + filtro.getTitulo() + "%"));
		}
		return predicates;
	}

}
