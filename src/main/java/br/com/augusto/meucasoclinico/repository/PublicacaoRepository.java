package br.com.augusto.meucasoclinico.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.augusto.meucasoclinico.model.Publicacao;
import br.com.augusto.meucasoclinico.repository.customer.PublicacaoRepositoryCustomer;

public interface PublicacaoRepository extends JpaRepository<Publicacao, Long>, PublicacaoRepositoryCustomer {

	Page<Publicacao> findByAutorDadosNome(String nomeAutor, Pageable paginacao);
	
}
