package br.com.augusto.meucasoclinico.repository.customer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.augusto.meucasoclinico.filtro.FiltroListaPublicacao;
import br.com.augusto.meucasoclinico.model.Publicacao;

public interface PublicacaoRepositoryCustomer {

	Page<Publicacao> getListaPaginada(FiltroListaPublicacao filtro, Pageable pageable);

}
